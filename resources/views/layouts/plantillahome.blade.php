<!DOCTYPE html>
<html lang="es">
<head>
    <title>@yield('tittle')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="shortcut icon" href="{{asset('multimedia/120.png')}}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('multimedia/120.png')}}" /> 
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('multimedia/152.png')}}" />
    <link rel="stylesheet" href="{{asset('css/stylenavbar.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylecards.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylebody.css')}}">
    <link rel="stylesheet" href="{{asset('css/fontawesome-free/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylecarousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylefooter.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/jquery-ui/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/modonocturno.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylecorrec.css')}}">
    <link rel="stylesheet" href="{{asset('css/btnflotante.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link rel="stylesheet" href="{{asset('css/anicollectionamantoli.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('pluggins/DataTables/datatables.min.css')}}"/>
    @yield('css')
  </head>
<body>
<style>

</style>
<nav class="amatoli-bar navbar navbar-expand-lg navbar-light bg-light sticky-top">
  <a class="navbar-brand" href="/"><img src="{{asset('multimedia/logo152.png')}}" height="55" alt="logo amatoli"></a>
  <div class="d-block d-sm-block d-md-none">
    <a class="nav-link amatoli-textos-bar card-texto @if(request()->is('carrito')) active activado @endif" href="{{route('carrito')}}">
      0<i class="iconos-amatoli fas fa-shopping-cart"></i>
    </a>
  </div>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link amatoli-textos-bar @if(request()->is('/')) active activado @endif" href="/">Inicio <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link amatoli-textos-bar @if(request()->is('tienda')) active activado @endif" href="/tienda">Tienda</a>
      </li>
      <li class="nav-item">
        <a class="nav-link amatoli-textos-bar @if(request()->is('contacto')) active activado @endif" href="/contacto">Contactanos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link amatoli-textos-bar @if(request()->is('nosotros')) active activado @endif" href="/nosotros">Nosotros</a>
      </li>
      @guest
        @if (Route::has('login'))
        <li class="nav-item">
            <a class="nav-link amatoli-textos-bar @if(request()->is('login')) active activado @endif" href="{{ route('login') }}">{{ __('Iniciar sesión') }}</a>
        </li>
    @endif
    @if (Route::has('register'))
        <li class="nav-item">
            <a class="nav-link amatoli-textos-bar @if(request()->is('register')) active activado @endif" href="{{ route('register') }}">{{ __('Registrarme') }}</a>
        </li>
    @endif
  @else
    <li class="nav-item dropdown amatoli-textos-bar">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->name }}
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a href="/home" class="dropdown-item">Mi cuenta</a>
            <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                {{ __('Cerrar sesión') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </li>
  @endguest
  <li class="nav-item d-none d-sm-none d-md-block">
    <a class="nav-link amatoli-textos-bar @if(request()->is('carrito')) active activado @endif" href="{{route('carrito')}}">0<i class="iconos-amatoli fas fa-shopping-cart"></i></a>
  </li>
    </ul>
    <form class="form-inline my-2 my-lg-0" action="/buscar/productos" method="GET">
      <input class="form-control" onkeyup="this.value=NumText(this.value)" maxlength="30" type="search" name="buscar" id="buscar" required placeholder="Buscar..." aria-label="Search">
      <div class="input-group-append">
        <button class="btn boton-primario  my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
      </div>
      @if ($errors->any())
            @foreach ($errors->all() as $error)
                <script>
                  alert('{{ $error }}')
                </script>
            @endforeach
      @endif
    </form>
    &nbsp;
    <button type="button" class="btn boton-primario" data-toggle="modal" data-target="#modalbusquedaav">
      <i class="fas fa-filter"></i>
    </button>
  </div>
</nav>

<div class="modal fade" id="modalbusquedaav" tabindex="-1" aria-labelledby="iniciosesionlabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="iniciosesionlabel">Busqueda avanzada</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-inline my-2 my-lg-0" action="/buscar/busquedaavanzada" method="GET">
          <div class=""> 
            <label for="nombrep">Nombre del producto</label>
            <input class="form-control" maxlength="30" type="text" name="nombre" id="nombrep" required placeholder="Nombre del producto" aria-label="Search">
          </div>
          &nbsp;
          <div>
              <label for="idcat">Categoria</label>
              <select name="categorias" class="form-control" id="idcat">
                <option value="1">Madera</option>
                <option value="2">Barro</option>
                <option value="3">Textiles</option>
              </select>
          </div>
          <div>
            <label for="idprecio">Precio Maximo</label>
            <input type="number" maxlength="5" class="form-control" placeholder="$0" name="precio" min="0" id="idprecio">
          </div>
          &nbsp;
          <div class="input-group-append">
            <button class="btn boton-primario  my-2 my-sm-0" type="submit"><i class="fas fa-search"></i>Busqueda avanzada</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
    @yield('content')
<footer class="site-footer footerclass">
  <a class="btn-flotante" id="tema"><i id="result"></i></a>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>Acerca de nosotros</h6>
            <p class="text-justify">
              Artesanias de la huasteca <i>Amatoli</i> 
              es una empresa comprometida con los artesanos hidalguenses para mejorar la valoración de sus productos.
              Buscamos dar a conocer la riqueza cultural de México. Es necesario que las artesanías sean reconocidas 
              y que formen parte de lo que nos hace sentir más orgullosos, 
              de lo que nos hace ser mexicanos.
            </p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Dirección</h6>
            <ul class="footer-links">
              <li><i class="fas fa-home"></i> Colonia centro, Av. Reloj, Huejutla de Reyes, Hidalgo, 43080, México.</li>
              <li><i class="fas fa-phone"></i> 7712674902</li>
              <li><i class="fas fa-phone"></i> 7712390974</li>
              <li><i class="fas fa-phone"></i> 7711795277</li>
              <li><a href="#"><i class="fas fa-envelope"></i> 20181140@uthh.edu.mx</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Información</h6>
            <ul class="footer-links">
              <li><a href="/quienessomos">Acerca de nosotros</a></li>
              <li><a href="/contacto">Contactanos</a></li>
              <li><a href="/avisodeprivacidad">Politica de privacidad</a></li>
              <li><a href="#">Preguntas frecuentes</a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">
              AMATOLI &copy; 2020 . Todos los derechos reservados. 
              <a href="#">Términos y condiciones</a> |
              <a href="/avisodeprivacidad">Aviso de Privacidad</a> |
              <a href="#">Política de devoluciones</a>
            </p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fab fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
              <li><a class="dribbble" href="#"><i class="fab fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
</footer>    
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    {{--<script type="text/javascript" src="{{asset('js/jquery-3.5.1.slim.min.js')}}"></script>--}}
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/right-click-disable.js')}}"></script>
    {{--<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>--}}
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('vendor/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/modonocturno.js')}}"></script>
    <script src="{{asset('js/animacionesamantoli.js')}}"></script>
    <script src="{{asset('js/scrollamantoli.js')}}"></script>
    <script src="{{asset('js/amantoli-helper-dom.js')}}"></script>
    <script src="{{asset('pluggins/DataTables/datatables.min.js')}}"></script>
    <script src="{{asset('js/amantoli-validaciones.js')}}"></script>
  <script>
  $( function() {
    $( "#buscar" ).autocomplete({
      source: function (request, response){
        $.ajax({
          url:"/autocompletar/productos",
          dataType:"json",
          data:{
            term: request.term
          },
          success:function(data){
            response(data)
          }
        });
      }
    });
  } );
  </script>
  @yield('js')
</body>
</html> 