<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="shortcut icon" href="{{asset('multimedia/logo.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('css/stylenavbar.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylecards.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylebody.css')}}">
    <link rel="stylesheet" href="{{asset('css/fontawesome-free/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylecarousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylefooter.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/jquery-ui/jquery-ui.min.css')}}">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body>
    <style>
        .boton-drop{
        color: #333333;
        }
        .boton-drop:hover{
        color: #5a1613;
        }
        .amatoli-link{
            color: #333333;
        }
        .amatoli-link:hover{
            color: #5a1613;
        }
    </style>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif
                            
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <footer class="site-footer">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-6">
              <h6>Acerca de nosotros</h6>
              <p class="text-justify">
                Artesanias de la huasteca <i>Amatoli</i> 
                es una empresa comprometida con los artesanos hidalguenses para mejorar la valoración de sus productos.
                Buscamos dar a conocer la riqueza cultural de México. Es necesario que las artesanías sean reconocidas 
                y que formen parte de lo que nos hace sentir más orgullosos, 
                de lo que nos hace ser mexicanos.
              </p>
            </div>
  
            <div class="col-xs-6 col-md-3">
              <h6>Dirección</h6>
              <ul class="footer-links">
                <li><i class="fas fa-home"></i> Colonia centro, Av. Reloj, Huejutla de Reyes, Hidalgo, 43080, México.</li>
                <li><i class="fas fa-phone"></i> 7712674902</li>
                <li><i class="fas fa-phone"></i> 7712390974</li>
                <li><i class="fas fa-phone"></i> 7711795277</li>
                <li><a href="#"><i class="fas fa-envelope"></i> 20181140@uthh.edu.mx</a></li>
              </ul>
            </div>
  
            <div class="col-xs-6 col-md-3">
              <h6>Información</h6>
              <ul class="footer-links">
                <li><a href="/quienessomos">Acerca de nosotros</a></li>
                <li><a href="/contacto">Contactanos</a></li>
                <li><a href="/avisodeprivacidad">Politica de privacidad</a></li>
                <li><a href="#">Preguntas frecuentes</a></li>
              </ul>
            </div>
          </div>
          <hr>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
              <p class="copyright-text">
                AMATOLI &copy; 2020 . Todos los derechos reservados. 
                <a href="#">Términos y condiciones</a> |
                <a href="/avisodeprivacidad">Aviso de Privacidad</a> |
                <a href="#">Política de devoluciones</a>
              </p>
            </div>
  
            <div class="col-md-4 col-sm-6 col-xs-12">
              <ul class="social-icons">
                <li><a class="facebook" href="#"><i class="fab fa-facebook"></i></a></li>
                <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a class="dribbble" href="#"><i class="fab fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
  </footer>    
    <link rel="stylesheet" href="{{asset('js/bootstrap.min.js')}}">
</body>
</html>
