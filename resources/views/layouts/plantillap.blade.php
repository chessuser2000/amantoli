<!DOCTYPE html>
<html lang="es">
<head>
    <title>@yield('tittle')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="shortcut icon" href="{{asset('multimedia/logo.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('css/stylenavbar.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylecards.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylebody.css')}}">
    <link rel="stylesheet" href="{{asset('css/fontawesome-free/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylecarousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylefooter.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/jquery-ui/jquery-ui.min.css')}}">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  </head>
<body>
<style>
    .boton-drop{
    color: #333333;
    }
    .boton-drop:hover{
    color: #5a1613;
    }
    .amatoli-link{
        color: #333333;
    }
    .amatoli-link:hover{
        color: #5a1613;
    }
</style>
<nav class="amatoli-bar navbar navbar-expand-lg sticky-top" style="height: 60px">
  <a class="navbar-brand" href="/"><img src="{{asset('multimedia/logo.png')}}" height="55" alt="logo amatoli"></a>
  <div class="d-block d-sm-block d-md-none">
    <a class="nav-link amatoli-textos-bar" href="#">
      0<i class="iconos-amatoli fas fa-shopping-cart"></i>
    </a>
  </div>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <i class="iconos-amatoli fas fa-bars"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link amatoli-textos-bar" href="/">Inicio <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link amatoli-textos-bar " href="/tienda">Tienda</a>
      </li>
      <li class="nav-item">
        <a class="nav-link amatoli-textos-bar" href="/contacto">Contactanos</a>
      </li>
      <li class="nav-item">
        <a href="{{route('aboutus')}}" class="nav-link amatoli-textos-bar">¿Quienes somos?</a>
      </li>
      <li class="nav-item dropdown amatoli-textos-bar">
        <a class="nav-link dropdown-toggle boton-drop" href="#" id="sesiones" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-tie iconos-amatoli"></i>
        </a>
        @guest
        <div class="dropdown-menu" aria-labelledby="sesiones">
            <a class="dropdown-item" href="#">
              <button type="button" class="btn btn-link amatoli-link" data-toggle="modal" data-target="#modaliniciosesion">
                  Iniciar sesión
              </button>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/registro">Registrarse</a>
          </div>
        @endguest
      </li>
    </ul>
    <ul class="navbar-nav mr-auto d-none d-sm-none d-md-block"> 
      <li class="nav-item">
        <a class="nav-link amatoli-textos-bar" href="#">0<i class="iconos-amatoli fas fa-shopping-cart"></i></a>
      </li>
    </ul>
    
    <form class="form-inline my-2 my-lg-0" action="/buscar/productos" method="GET">
      <input class="form-control" type="search" name="buscar" id="buscar" required placeholder="Buscar..." aria-label="Search">
      <div class="input-group-append">
        <button class="btn boton-primario  my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
      </div>
    </form>
    &nbsp;
    <button type="button" class="btn boton-primario" data-toggle="modal" data-target="#modalbusquedaav">
      <i class="fas fa-filter"></i>
    </button>
  </div>
</nav>
<div class="modal fade" id="modaliniciosesion" tabindex="-1" aria-labelledby="iniciosesionlabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="iniciosesionlabel">Iniciar sesión</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn boton-primario">Iniciar sesión</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalbusquedaav" tabindex="-1" aria-labelledby="iniciosesionlabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="iniciosesionlabel">Busqueda avanzada</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-inline my-2 my-lg-0" action="/buscar/busquedaavanzada" method="GET">
          <div class=""> 
            <label for="nombrep">Nombre del producto</label>
            <input class="form-control" type="text" name="nombre" id="nombrep" required placeholder="Nombre del producto" aria-label="Search">
          </div>
          &nbsp;
          <div>
              <label for="idcat">Categoria</label>
              <select name="categorias" class="form-control" id="idcat">
                <option value="1">Madera</option>
                <option value="2">Barro</option>
                <option value="3">Textiles</option>
              </select>
          </div>
          <div>
            <label for="idprecio">Precio Maximo</label>
            <input type="number" class="form-control" placeholder="$0" name="precio" min="0" id="idprecio">
          </div>
          &nbsp;
          <div class="input-group-append">
            <button class="btn boton-primario  my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
    @yield('content')
<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>Acerca de nosotros</h6>
            <p class="text-justify">
              Artesanias de la huasteca <i>Amatoli</i> 
              es una empresa comprometida con los artesanos hidalguenses para mejorar la valoración de sus productos.
              Buscamos dar a conocer la riqueza cultural de México. Es necesario que las artesanías sean reconocidas 
              y que formen parte de lo que nos hace sentir más orgullosos, 
              de lo que nos hace ser mexicanos.
            </p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Dirección</h6>
            <ul class="footer-links">
              <li><i class="fas fa-home"></i> Colonia centro, Av. Reloj, Huejutla de Reyes, Hidalgo, 43080, México.</li>
              <li><i class="fas fa-phone"></i> 7712674902</li>
              <li><i class="fas fa-phone"></i> 7712390974</li>
              <li><i class="fas fa-phone"></i> 7711795277</li>
              <li><a href="#"><i class="fas fa-envelope"></i> 20181140@uthh.edu.mx</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Información</h6>
            <ul class="footer-links">
              <li><a href="/quienessomos">Acerca de nosotros</a></li>
              <li><a href="/contacto">Contactanos</a></li>
              <li><a href="/avisodeprivacidad">Politica de privacidad</a></li>
              <li><a href="#">Preguntas frecuentes</a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">
              AMATOLI &copy; 2020 . Todos los derechos reservados. 
              <a href="#">Términos y condiciones</a> |
              <a href="/avisodeprivacidad">Aviso de Privacidad</a> |
              <a href="#">Política de devoluciones</a>
            </p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fab fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
              <li><a class="dribbble" href="#"><i class="fab fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
</footer>    
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <!--<script type="text/javascript" src="{{asset('js/jquery-3.5.1.slim.min.js')}}"></script>-->
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/right-click-disabled.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('vendor/jquery-ui/jquery-ui.min.js')}}"></script>
  <script>
  $( function() {
    $( "#buscar" ).autocomplete({
      source: function (request, response){
        $.ajax({
          url:"/autocompletar/productos",
          dataType:"json",
          data:{
            term: request.term
          },
          success:function(data){
            response(data)
          }
        });
      }
    });
  } );
  </script>
</body>
</html> 