@extends('layouts.plantillahome')
@if (Auth::user()->Tipo_user=="admin") @section('tittle','Amantoli | Productos') @else @section('tittle','Pagína no encontrada')@endif

@section('content')
@if (Auth::user()->Tipo_user=="admin")
    <div class="container-fluid" id="body">
        <div class="container">
            <div class="table-responsive">
                <table class="table table-striped table-hover" id="productos">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Tienda</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Más</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($productos as $prod)
                        <tr>
                            <th scope="row">
                                {{--@for ($i = 0; $i < $prod->count(); $i++)
                                    {{$i}}
                                @endfor--}}
                                -
                            </th>
                            <td>{{$prod->vchNombre}}</td>
                            <td>{{$prod->intID_Categoria}}</td>
                            <td>{{$prod->intID_Tienda}}</td>
                            <td>{{$prod->fltPrecioV}}</td>
                            <td>
                                <a href="{{route('addimages',$prod->intID_Producto)}}" class="btn btn-primary"><i class="far fa-images"></i></a>
                                <a href="" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                <form action="{{route('eliminarprod',$prod->intID_Producto)}}" class="d-inline formulario-eliminar" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            
        </div>
        <br>
    </div>
    @section('js')
        <script>
            $(document).ready(function() {
                $('#productos').DataTable({
                    dom: 'Bfrtip',
                    lengthMenu: [
                        [ 10, 25, 50, -1 ],
                        [ '10 filas', '25 filas', '50 filas', 'Mostrar todo' ]
                    ],
                    buttons: [
                        'colvis',
                        'excel',
                        'print',
                        'pdf',
                        'pageLength'
                    ],
                    "language": {
                        "url": "{{asset('pluggins/Datatables/Spanish.json')}}"
                    }
                });
            } );
        </script>
        <script>
            $('.formulario-eliminar').submit(function(e){
              e.preventDefault();
              
              if (confirm("¿Estas seguro de eliminar este producto?")) {
                    this.submit();
                }
          });
          </script>
    @endsection
    @else
        @include('errors.404')
    @endif
@endsection