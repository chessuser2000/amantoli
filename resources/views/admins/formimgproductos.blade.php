@extends('layouts.plantillahome')
@section('tittle','Agregar imagenes al producto')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.6/dropzone.min.css" integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A==" crossorigin="anonymous" />
@endsection
@section('content')
    <div class="container-fluid" id="body">
        <div class="container">
            <h1 class="amatoli-textos subtitulo" id="titulos" style="text-align: center">IMAGENES PRODUCTOS</h1>
            <h2 class="amantoli-textos" style="text-align: center">{{$productos->vchNombre}}</h2>
            <form action="{{route('guardarimgp',$productos->intID_Producto)}}">
                <label for="">Imagen principal</label>
                <input type="file" name="imgp" class="form-control">
                <button type="submit" class="btn btn-success">Guardar</button>
            </form>
            <br><br>
            <form action="{{route('guardarimg',$productos->intID_Producto)}}" method="POST" class="dropzone" id="my-awesome-dropzone"></form>
            <br><br>
        </div>
    </div>
    <div class="container"> 
        <div class="row">
            <div class="col">
                <div class="card-columns">
                    <div id="imagenes">
                        @foreach ($imgs as $img)
                            <div class="card">
                                <img src="{{asset($img->vchImagen)}}" class="card-img-top" alt="">
                                <div class="card-body">
                                    <p class="card-text">
                                        <form action="{{route('eliminarimg',$img->intID_imgprod)}}" class="d-inline formulario-eliminar" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger"><i class="fas fa-window-close"></i></button>
                                        </form>
                                    </p>
                                </div>
                            </div>
                         @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.6/min/dropzone.min.js" integrity="sha512-KgeSi6qqjyihUcmxFn9Cwf8dehAB8FFZyl+2ijFEPyWu4ZM8ZOQ80c2so59rIdkkgsVsuTnlffjfgkiwDThewQ==" crossorigin="anonymous"></script>
    <script>
        Dropzone.options.myAwesomeDropzone = {
            headers:{
                'X-CSRF-TOKEN':"{{csrf_token()}}"
            },
            dictDefaultMessage:"Arrastre una o varias imagenes aquí",
            acceptedFiles:"image/*",
            maxFilesize:2,
            paramName:'imagen',
        };
    </script>
@endsection