@extends('layouts.plantillahome')
@if (Auth::user()->Tipo_user=="admin") @section('tittle','Crear productos') @else @section('tittle','Pagína no encontrada')@endif

@section('content')
@if (Auth::user()->Tipo_user=="admin")
    <div class="container-fluid" id="body">
        <div class="container">
            <h1 class="amatoli-textos subtitulo" id="titulos" style="text-align: center">PRODUCTOS</h1>
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
             @endif
            <form action="/createproducto" method="post" enctype="multipart/form-data">
                @csrf
                <label for="idnombre">Nombre del producto</label>
                <input type="text" name="nombreprod" placeholder="Ej: carrito de madera" class="form-control" id="idnombre">
                <div class="row">
                    <div class="col-md-6">
                        <label for="idtienda">Tienda</label>
                        <select name="tienda" id="idtienda" class="form-control">
                            @foreach ($tiendas as $tienda)
                                <option value="{{$tienda->intID_Tienda}}">{{$tienda->vchNombreTienda}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="idcat">Categoria</label>
                        <select class="form-control" name="categoria" id="idcat">
                            @foreach ($categorias as $cat)
                                <option value="{{$cat->intID_Cat}}">{{$cat->vchCategoria}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="idprecio">Precio fabrica</label>
                        <input type="number" class="form-control" name="preciov" placeholder="$0" id="idprecio">
                    </div>
                    <div class="col-md-3">
                        <label for="idganancia">Ganancia en procentaje</label>
                        <input type="number" class="form-control" name="ganancia" id="idganancia" placeholder="%0">
                    </div>
                    <div class="col-md-3">
                        <label for="idexistencia">Existencia</label>
                        <input type="number" class="form-control" name="existencia" id="idexitencia" placeholder="0">
                    </div>
                    <div class="col-md-3">
                        <label for="idedo">Estado</label>
                        <select name="estado" class="form-control" id="idedo">
                            <option value="NoDisponible">No disponible</option>
                            <option value="Disponible"> Disponible</option>
                        </select>
                    </div>
                </div>
                <label for="iddescripcion">Descripción</label>
                <textarea name="descripcion" id="iddescripcion" class="form-control" placeholder="Escriba una breve descripción del producto..." cols="30" rows="7"></textarea>
                <br>
                <button type="reset" class="btn btn-danger">
                    Limpiar formulario <i class="fas fa-undo-alt"></i>
                </button>
                <button type="submit" class="btn boton-primario">
                    Guardar producto <i class="far fa-save"></i>
                </button>
            </form>
        </div>
        <br>
    </div>
    <script>
        var ganancia = document.getElementById('idcat');
    </script>
    @else
        @include('errors.404')
    @endif
@endsection