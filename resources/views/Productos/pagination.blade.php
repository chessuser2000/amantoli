@foreach ($productos as $prod)
<div class="card tarjeta-contenedora" style="width: 13rem;" data-anijs="if: click, do: bounceIn animated, to: $ancestors target | .container-box">
  <a href="{{route('detalleproducto',$prod->intID_Producto)}}"><img src="{{asset('multimedia/P1.png')}}" class="card-img-top img-tarjeta-prod" alt="articulos-tortillero"></a>
  <div class="card-body cuerpo-tarjeta">
    <h5 class="card-title"><a href="{{route('detalleproducto',$prod->intID_Producto)}}" class="titulo-prod card-texto">{{$prod->vchNombre}}</a></h5>
    <p class="card-text card-texto">${{$prod->fltPrecioV}}</p>
    <a href="/addtocart?{{$prod->intID_Producto}}" class="link-tarjeta">Añadir al carrito</a>
  </div>
</div>
@endforeach