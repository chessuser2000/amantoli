@extends('layouts.plantillahome')
@section('tittle', 'Tienda')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Inicio</a></li>
      <li class="breadcrumb-item active" aria-current="page">Resultados</li>
    </ol>
</nav>
<div class="container-fluid contenedor-padd" id="body">
<h1 class="amatoli-textos subtitulo" style="text-align: center">PRODUCTOS</h1>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <aside>
          <div class="card">
            <article class="card-group-item">
              <header class="card-header">
                <h6 class="title">Ordenar por</h6>
              </header>
              <div class="filter-content">
                <div class="card-body">
                <div class="form-row">
                <div class="form-group col-md-12">
                  <select name="" id="" class="form-control">
                    <option value="">Más popular</option>
                    <option value="">Lo más nuevo</option>
                    <option value="">Más barato</option>
                    <option value="">Más caro</option>
                  </select>
                </div>
                </div>
                </div> <!-- card-body.// -->
              </div>
            </article>
            <article class="card-group-item">
              <header class="card-header">
                <h6 class="title">Rango de precio</h6>
              </header>
              <div class="filter-content">
                <div class="card-body">
                <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Min</label>
                  <input type="number" min="0" class="form-control" id="min" placeholder="$ min">
                </div>
                <div class="form-group col-md-6 text-right">
                  <label>Max</label>
                  <input type="number" min="0" class="form-control" id="max" placeholder="$ max">
                </div>
                </div>
                </div> <!-- card-body.// -->
              </div>
            </article> <!-- card-group-item.// -->
            <article class="card-group-item">
              <header class="card-header">
                <h6 class="title">Categorias </h6>
              </header>
              <div class="filter-content">
                <div class="card-body">
                  <div class="custom-control custom-checkbox">
                    <span class="float-right badge badge-light round">52</span>
                      <input type="checkbox" class="custom-control-input" id="Check1">
                      <label class="custom-control-label" for="Check1">Barro</label>
                  </div> <!-- form-check.// -->
          
                  <div class="custom-control custom-checkbox">
                    <span class="float-right badge badge-light round">132</span>
                      <input type="checkbox" class="custom-control-input" id="Check2">
                     <label class="custom-control-label" for="Check2">Madera</label>
                  </div> <!-- form-check.// -->
          
                  <div class="custom-control custom-checkbox">
                    <span class="float-right badge badge-light round">17</span>
                      <input type="checkbox" class="custom-control-input" id="Check3">
                      <label class="custom-control-label" for="Check3">Textil</label>
                  </div> <!-- form-check.// -->
          
                  <div class="custom-control custom-checkbox">
                    <span class="float-right badge badge-light round">7</span>
                      <input type="checkbox" class="custom-control-input" id="Check4">
                      <label class="custom-control-label" for="Check4">Otros materiales</label>
                  </div> <!-- form-check.// -->
                </div> <!-- card-body.// -->
              </div>
            </article> <!-- card-group-item.// -->
          </div> <!-- card.// -->
        </aside>
      </div>
      <div class="col-sm-9" id="productos">
        @foreach ($productos as $prod)
            <div class="card tarjeta-contenedora" style="width: 13rem;">
            <a href="{{route('detalleproducto',$prod->intID_Producto)}}"><img src="{{asset('multimedia/P1.png')}}" class="card-img-top img-tarjeta-prod" alt="articulos-tortillero"></a>
            <div class="card-body cuerpo-tarjeta">
                <h5 class="card-title"><a href="{{route('detalleproducto',$prod->intID_Producto)}}" class="titulo-prod">{{$prod->vchNombre}}</a></h5>
                <p class="card-text">${{$prod->fltPrecioV}}</p>
                <a href="#" class="link-tarjeta">Añadir al carrito</a>
            </div>
            </div>
        @endforeach
        <div class="bg-gray-50 px-6 py-4 border-t border-gray-200" >
            {{$productos->links()}}
        </div>
      </div>
    </div>
  </div>  
</div>
@endsection
