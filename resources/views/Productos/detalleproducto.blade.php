@extends('layouts.plantillahome')
@section('tittle', $detprod->vchNombre)
@section('css')
  <link rel="stylesheet" type="text/css" href="{{asset('pluggins/Fotorama/fotorama.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{asset('css/detalleprod.css')}}"/>
@endsection
@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Inicio</a></li>
    <li class="breadcrumb-item"><a href="/tienda">Tienda</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{$detprod->vchNombre}}</li>
  </ol>
</nav>
    <div class="container-fluid contenedor-padd" id="body">
        <div class="container">
            <div class="row">
              <div class="col-md-8" style="margin-block-end: 10px">
                <div class="fotorama" data-nav="thumbs">
                  @foreach ($imgs as $img)
                  <a href="{{asset($img->vchImagen)}}"><img src="{{asset($img->vchImagen)}}"></a>
                  @endforeach
                </div>
              </div>

              <div class="col-md-4">
                  <small>Unidades vendidas: 22</small>
                  <div class="row">
                    <div class="col-md-8"><h1>{{$detprod->vchNombre}}</h1></div>
                    <div class="col-md-4">
                      @auth
                      <a href=""><i class="iconos-amatoli far fa-heart"></i></a>
                      @endauth
                    </div>
                  </div>
                  <div class="row filas-alineadas">
                      <div><strong>Calificación: </strong></div>
                      <div>
                          <form>
                              <p class="clasificacion">
                                <input id="radio1" type="radio" name="estrellas" value="5"><!--
                                --><label for="radio1">★</label><!--
                                --><input id="radio2" type="radio" name="estrellas" value="4"><!--
                                --><label for="radio2">★</label><!--
                                --><input id="radio3" type="radio" name="estrellas" value="3"><!--
                                --><label for="radio3">★</label><!--
                                --><input id="radio4" type="radio" name="estrellas" value="2"><!--
                                --><label for="radio4">★</label><!--
                                --><input id="radio5" type="radio" name="estrellas" value="1"><!--
                                --><label for="radio5">★</label>
                              </p>
                            </form>
                      </div>
                  </div>
                    <div class="row filas-alineadas">
                        <div><strong>Categoria: </strong> &nbsp;</div>
                        <div>{{$detprod->intID_Categoria}}</div>
                    </div>
                    <div class="row filas-alineadas">
                      <div><p class="precio-prod">$</p></div>
                      <div><p class="precio-prod">{{$detprod->fltPrecioV}}</p></div>
                  </div>
                  <div class="row filas-alineadas">
                    <div><strong>Cantidad:</strong>&nbsp;</div>
                    <div><input type="number" placeholder="0" class="form-control col-md-3" name="nprod" id=""></div>
                  </div>
                  <br>
                  <a href="" class="btn boton-primario">Agregar al carrito de compras</a>
                  <br><br>
                  <strong>Vendido por:</strong>
                  <a href="" class="link-tarjeta">{{$detprod->intID_Tienda}}</a>
                  <br><br>
                  <strong>Descripción del producto: </strong>
                  <p style="text-align: justify">{{$detprod->txtDescripcion}}</p>
              </div>
          </div>
        </div>
        
        <div class="container">
          <h1 style="text-align: center">Más productos similares</h1>
          @include('Productos.pagination')
        </div>
        <div class="container">
          <h2 class="amatoli-textos subtitulo">Comentarios</h2>
          <ul class="list-group list-group-flush">
            <li class="list-group-item comentarios">
              <form>
                <p class="clasificacion">
                  <input id="radio1" type="radio" name="estrellas" value="5"><!--
                  --><label for="radio1">★</label><!--
                  --><input id="radio2" type="radio" name="estrellas" value="4"><!--
                  --><label for="radio2">★</label><!--
                  --><input id="radio3" type="radio" name="estrellas" value="3"><!--
                  --><label for="radio3">★</label><!--
                  --><input id="radio4" type="radio" name="estrellas" value="2"><!--
                  --><label for="radio4">★</label><!--
                  --><input id="radio5" type="radio" name="estrellas" value="1"><!--
                  --><label for="radio5">★</label>
                </p>
              </form>
              <h5 class="">Muy bueno</h5>
              <p class="">El producto es bueno de alta calidad, viene empaquetado muy bien me encantó solo 
                que tengo un inconveniente ya que el instructivo es de otro reloj no viene bien 
                explicado como acortar el extensible apesar de que trae la herramienta para hacerlo, 
                si alguien sabe puede explicarme?.</p>
            </li>
            <li class="list-group-item comentarios">
              <form>
                <p class="clasificacion">
                  <input id="radio1" type="radio" name="estrellas" value="5"><!--
                  --><label for="radio1">★</label><!--
                  --><input id="radio2" type="radio" name="estrellas" value="4"><!--
                  --><label for="radio2">★</label><!--
                  --><input id="radio3" type="radio" name="estrellas" value="3"><!--
                  --><label for="radio3">★</label><!--
                  --><input id="radio4" type="radio" name="estrellas" value="2"><!--
                  --><label for="radio4">★</label><!--
                  --><input id="radio5" type="radio" name="estrellas" value="1"><!--
                  --><label for="radio5">★</label>
                </p>
              </form>
              <h5>Me sorprendió</h5>
              <p>Tiene calidad y funciona, esos que dicen que las otras carátulas no sirven nunca 
                han usado un cronómetro, los materiales son buenos y trae hasta la herramienta para 
                acortar el extensible, además de venir muy bien empacado en caja de calidad.</p>
            </li>
          </ul>
        </div>
        <br>
    </div>
@endsection
@section('js')
<script type="text/javascript" src="{{asset('pluggins/Fotorama/fotorama.js')}}"></script>
@endsection