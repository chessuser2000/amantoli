@extends('layouts.plantillahome')
@section('tittle', 'Tienda')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Inicio</a></li>
      <li class="breadcrumb-item active" aria-current="page">Tienda</li>
    </ol>
</nav>
<div class="container-fluid" id="body" style="margin-top: -15px">
<h1 class="amatoli-textos subtitulo" id="titulos" style="text-align: center">PRODUCTOS</h1>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <aside>
          <div class="card">
            <article class="card-group-item">
              <header class="card-header">
                <h6 class="title">Ordenar por</h6>
              </header>
              <div class="filter-content">
                <div class="card-body">
                <div class="form-row">
                <div class="form-group col-md-12">
                  <select name="" id="" class="form-control">
                    <option value="">Más popular</option>
                    <option value="">Lo más nuevo</option>
                    <option value="">Más barato</option>
                    <option value="">Más caro</option>
                  </select>
                </div>
                </div>
                </div> <!-- card-body.// -->
              </div>
            </article>
            <article class="card-group-item">
              <header class="card-header">
                <h6 class="title">Rango de precio</h6>
              </header>
              <div class="filter-content">
                <div class="card-body">
                <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Min</label>
                  <input type="number" min="0" class="form-control" id="min" placeholder="$ min">
                </div>
                <div class="form-group col-md-6 text-right">
                  <label>Max</label>
                  <input type="number" min="0" class="form-control" id="max" placeholder="$ max">
                </div>
                </div>
                </div> <!-- card-body.// -->
              </div>
            </article> <!-- card-group-item.// -->
            <article class="card-group-item">
              <header class="card-header">
                <h6 class="title">Categorias </h6>
              </header>
              <div class="filter-content">
                <div class="card-body">
                  <div class="custom-control custom-checkbox">
                    <span class="float-right badge badge-light round">52</span>
                      <input type="checkbox" class="custom-control-input" id="Check1">
                      <label class="custom-control-label" for="Check1">Barro</label>
                  </div> <!-- form-check.// -->
          
                  <div class="custom-control custom-checkbox">
                    <span class="float-right badge badge-light round">132</span>
                      <input type="checkbox" class="custom-control-input" id="Check2">
                     <label class="custom-control-label" for="Check2">Madera</label>
                  </div> <!-- form-check.// -->
          
                  <div class="custom-control custom-checkbox">
                    <span class="float-right badge badge-light round">17</span>
                      <input type="checkbox" class="custom-control-input" id="Check3">
                      <label class="custom-control-label" for="Check3">Textil</label>
                  </div> <!-- form-check.// -->
          
                  <div class="custom-control custom-checkbox">
                    <span class="float-right badge badge-light round">7</span>
                      <input type="checkbox" class="custom-control-input" id="Check4">
                      <label class="custom-control-label" for="Check4">Otros materiales</label>
                  </div> <!-- form-check.// -->
                </div> <!-- card-body.// -->
              </div>
            </article> <!-- card-group-item.// -->
          </div> <!-- card.// -->
        </aside>
      </div>
      <div class="col-sm-9" id="productos">
        @include('Productos.pagination')
      </div>
    </div>
  </div>  
</div>
<script>
  let page = 4;
  window.onscroll = function(){
    if((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight){
      const section = document.getElementById('productos');
      fetch('/tienda/pagination?page='+page,{
        method:'get'
      }).then(function(response){
        return response.text();
      }).then(function(htmlContent){
        section.innerHTML+=htmlContent;
        console.log(htmlContent);
        page+=1;
      }).catch(function(err){
        console.log(err);
      });
    }
  }
</script> 
@endsection

   

