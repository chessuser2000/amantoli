@extends('layouts.plantillahome')
@section('tittle','Carrito de compras')
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Inicio</a></li>
      <li class="breadcrumb-item active" aria-current="page">Carrito de compras</li>
    </ol>
</nav>
    <div class="container-fluid contenedor-padd" id="body">
        <h1 class="amatoli-textos subtitulo" id="titulos" style="text-align: center">Carrito de compras</h1>
        <div class="container">
            <div class="table-responsive">
                <table class="table table-hover">
                    <tbody>
                      <tr>
                        <td><img src="{{asset('multimedia/P2.png')}}" class="img-fluid imagen-tabla" alt="articulos-amantoli"></td>
                        <td>Juego de canastas</td>
                        <td>
                            <form action="" method="post">
                                <input type="number" class="form-control col-md-3" placeholder="0" name="cantidad" id="idcantidad">
                            </form>
                        </td>
                        <td>$500.00</td>
                        <td><a href=""><i class="fas fa-trash-alt iconos-amatoli" style="font-size: 2em"></i></a></td>
                      </tr>
                      <tr>
                        <td><img src="{{asset('multimedia/P1.png')}}" class="img-fluid imagen-tabla" alt="articulos-amantoli"></td>
                        <td>Thornton</td>
                        <td>
                            <form action="" method="post">
                                <input type="number" class="form-control col-md-3" placeholder="0" name="cantidad" id="idcantidad">
                            </form>
                        </td>
                        <td>499.00</td>
                        <td><a href=""><i class="fas fa-trash-alt iconos-amatoli" style="font-size: 2em"></i></a></td>
                      </tr>
                      <tr>
                          <td>Envio a:</td>
                          <td>
                              <select class="form-control" name="" id="">
                                  <option value="">C.P.: 43054, Huautla</option>
                                  <option value=""><a href="#" >Añadir dirección</a></option>
                              </select>
                          </td>
                          <td>Subtotoal:</td>
                          <td>$999.00</td>
                          <td><a class="btn boton-primario" href="">Continuar compra</a></td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
        <br>
    </div>
@endsection