@extends('layouts.plantillahome')
@section('tittle', 'Inicio')
@section('content')
<div id="body">
<div id="amatoli-slider" class="carousel slide d-none d-sm-none d-md-block" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#amatoli-slider" data-slide-to="0" class="active"></li>
      <li data-target="#amatoli-slider" data-slide-to="1"></li>
      <li data-target="#amatoli-slider" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{asset('multimedia/carousel/1C.png')}}" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5 class="titulos-carousel">¡Síguenos en nuestras redes sociales!</h5>
          <p class="social-carousel">
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-facebook"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a>
          </p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('multimedia/carousel/2C.png')}}" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5 class="titulos-carousel">¡Síguenos en nuestras redes sociales!</h5>
          <p class="social-carousel">
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-facebook"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a>
          </p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('multimedia/carousel/3C.png')}}" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5 class="titulos-carousel">¡Síguenos en nuestras redes sociales!</h5>
          <p class="social-carousel">
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-facebook"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a>
          </p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#amatoli-slider" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Anterior</span>
    </a>
    <a class="carousel-control-next" href="#amatoli-slider" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Siguiente</span>
    </a>
  </div>

  <div id="amatoli-slider-movil" class="carousel slide d-block d-sm-block d-md-none" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#amatoli-slider-movil" data-slide-to="0" class="active"></li>
      <li data-target="#amatoli-slider-movil" data-slide-to="1"></li>
      <li data-target="#amatoli-slider-movil" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{asset('multimedia/carousel/4C.png')}}" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5 class="titulos-carousel">¡Síguenos en nuestras redes sociales!</h5>
          <p class="social-carousel">
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-facebook"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a>
          </p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('multimedia/carousel/5C.png')}}" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5 class="titulos-carousel">¡Síguenos en nuestras redes sociales!</h5>
          <p class="social-carousel">
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-facebook"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a>
          </p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('multimedia/carousel/6C.png')}}" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5 class="titulos-carousel">¡Síguenos en nuestras redes sociales!</h5>
          <p class="social-carousel">
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-facebook"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a>
            <a href="http://" target="_blank" class="iconos-amatoli" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a>
          </p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#amatoli-slider-movil" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Anterior</span>
    </a>
    <a class="carousel-control-next" href="#amatoli-slider-movil" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Siguiente</span>
    </a>
  </div>
<div class="container-fluid contenedor-padre">
    <h1 class="amatoli-textos subtitulo">CATEGORIAS</h1>
    <div class="card tarjeta-contenedora" style="width: 20rem;">
      <a href="{{route('detallecat', 1)}}"><img src="{{asset('multimedia/3.png')}}" class="card-img-top img-tarjeta-categoria" id="imgca" alt="articulos-barro"></a>
      <div class="card-body cuerpo-tarjeta">
        <h5 class="card-title card-texto">Barro</h5>
        <p class="card-text card-texto">Articulos hechos a base de barro.</p>
        <a href="{{route('detallecat', 1)}}" class="link-tarjeta">Ver todo</a>
      </div>
    </div>
    <div class="card tarjeta-contenedora" style="width: 20rem;">
      <a href="{{route('detallecat', 2)}}"><img src="{{asset('multimedia/4.png')}}" class="card-img-top img-tarjeta-categoria" alt="articulos-madera"></a>
      <div class="card-body cuerpo-tarjeta">
        <h5 class="card-title card-texto">Madera</h5>
        <p class="card-text card-texto">Articulos hechos a base de madera.</p>
        <a href="{{route('detallecat', 2)}}" class="link-tarjeta">Ver todo</a>
      </div>
    </div>
    <div class="card tarjeta-contenedora" style="width: 20rem;">
      <a href="{{route('detallecat', 3)}}"><img src="{{asset('multimedia/5.png')}}" class="card-img-top img-tarjeta-categoria" alt="articulos-textiles"></a>
      <div class="card-body cuerpo-tarjeta">
        <h5 class="card-title card-texto">Textiles</h5>
        <p class="card-text card-texto">Aticulos de ropa de la mejor calidad.</p>
        <a href="{{route('detallecat', 3)}}" class="link-tarjeta">Ver todo</a>
      </div>
    </div>
  </div>
  <!--Apartado de productos nuevos-->
  <div class="container contenedor-padre" data-anijs="if: scroll, on: window, do: zoomIn animated, before: $scrollReveal">
    <h1 class="amatoli-textos subtitulo">PRODUCTOS NUEVOS</h1>
    @foreach ($productos as $prod)
    <div class="card tarjeta-contenedora" style="width: 13rem;">
      <a href="{{route('detalleproducto',$prod->intID_Producto)}}">
        <img src="{{$prod->vchImagen}}" class="card-img-top img-tarjeta-prod" alt="articulos-tortillero">
      </a>
      <div class="card-body cuerpo-tarjeta">
        <h5 class="card-title"><a href="{{route('detalleproducto',$prod->intID_Producto)}}" class="titulo-prod card-texto">{{$prod->vchNombre}}</a></h5>
        <p class="card-text card-texto">${{$prod->fltPrecioV}}</p>
        <a href="#" class="link-tarjeta">Añadir al carrito</a>
      </div>
    </div>
    @endforeach
  </div>
  <br>
</div>
@endsection
