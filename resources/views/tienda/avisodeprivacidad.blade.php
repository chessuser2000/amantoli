@extends('layouts.plantillahome')
@section('tittle', 'Aviso de privacidad')

    <style>
        .container-aviso{
            margin-block-end: 10px;
        }
    </style>
    
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Inicio</a></li>
      <li class="breadcrumb-item active" aria-current="page">Politica de privacidad</li>
    </ol>
</nav>
<div class="container-fluid contenedor-padd" id="body">
<div class="container container-aviso">
    <h1>Política de privacidad</h1>
    <p>
    La presente política de privacidad establece los términos en que Amantoli usa y protege la
    información que es proporcionada por sus usuarios al momento de utilizar su sitio web.
    Esta compañía está comprometida con la seguridad de los datos de sus usuarios. Cuando le
    pedimos llenar los campos de información personal con la cual usted pueda ser
    identificado, lo hacemos asegurando que sólo se empleará de acuerdo con los términos de
    este documento. Sin embargo esta Política de Privacidad puede cambiar con el tiempo o ser
    actualizada por lo que le recomendamos y enfatizamos revisar continuamente esta página
    para asegurarse que está de acuerdo con dichos cambios.
    </p>
    <h1>Responsable de la protección de datos personales</h1>
    <p>
    Amantoli (Artesanía de las Huastecas), es responsable del tratamiento de sus datos
    personales.
    </p>
    <h1>Domicilio</h1>
    <p>
    Colonia centro, Av.Reloj, Huejutla De Reyes, Hgo, 43080, México.
    Preguntas e información de contacto
    Si quieres: acceder, corregir, enmendar o borrar cualquier información personal que
    poseamos sobre ti, registrar una queja, o simplemente quieres más información contacta a
    nuestro Oficial de Cumplimiento de Privacidad 201811@uthh.edu.mx o por
    correo postal a la siguiente dirección Colonia centro, Av. Reloj, Huejutla De Reyes, Hgo,
    43080, México.
    </p>
    <h1>Uso de la información recogida</h1>
    <p>
    Nuestro sitio web emplea la información con el fin de proporcionar el mejor servicio
    posible, particularmente para mantener un registro de usuarios, de pedidos en caso que
    aplique, y mejorar nuestros productos y servicios. Es posible que sean enviados correos
    electrónicos periódicamente a través de nuestro sitio con ofertas especiales, nuevos
    productos y otra información publicitaria que consideremos relevante para usted o que
    pueda brindarle algún beneficio, estos correos electrónicos serán enviados a la dirección
    que usted proporcione y podrán ser cancelados en cualquier momento.
    Amantoli está altamente comprometido para cumplir con el compromiso de mantener su
    información segura. Usamos los sistemas más avanzados y los actualizamos
    constantemente para asegurarnos que no exista ningún acceso no autorizado.
    </p>
    <h1>Información que es recogida</h1>
    <p>
    Nuestro sitio web podrá recoger información personal por ejemplo: Nombre, información
    de contacto como su dirección de correo electrónica e información demográfica. Así 
    mismo cuando sea necesario podrá ser requerida información específica para procesar
    algún pedido o realizar una entrega o facturación.
    Estos son los tipos de datos que recolectamos:
    Información que nos proporcionas directamente al registrarte o utilizar nuestros servicios:
    <ol>
        <li>Nombre</li>
        <li>Imagen personal (foto personal o foto del documento)</li>
        <li>Información de contacto (como número de teléfono, domicilio, dirección de correo
    electrónico)</li>
    </ol>
    </p>
    <h1>Cookies</h1>
    <p>
    Una cookie se refiere a un fichero que es enviado con la finalidad de solicitar permiso para
    almacenarse en su ordenador, al aceptar dicho fichero se crea y la cookie sirve entonces
    para tener información respecto al tráfico web, y también facilita las futuras visitas a una
    web recurrente. Otra función que tienen las cookies es que con ellas las web pueden
    reconocerte individualmente y por tanto brindarte el mejor servicio personalizado de su
    web.
    Nuestro sitio web emplea las cookies para poder identificar las páginas que son visitadas y
    su frecuencia. Esta información es empleada únicamente para análisis estadístico y después
    la información se elimina de forma permanente. Usted puede eliminar las cookies en
    cualquier momento desde su ordenador. Sin embargo las cookies ayudan a proporcionar un
    mejor servicio de los sitios web, estás no dan acceso a información de su ordenador ni de
    usted, a menos de que usted así lo quiera y la proporcione directamente, Usted puede
    aceptar o negar el uso de cookies, sin embargo la mayoría de navegadores aceptan cookies
    automáticamente pues sirve para tener un mejor servicio web. También usted puede
    cambiar la configuración de su ordenador para declinar las cookies. Si se declinan es
    posible que no pueda utilizar algunos de nuestros servicios.
    </p>
    <h1>Consentimiento</h1>
    <p>
    ¿Cómo obtienen mi consentimiento?
    Cuando nos provees tu información personal para completar una transacción, verificar tu
    tarjeta de crédito, crear una orden, concertar un envío o hacer una devolución, implicamos
    que aceptas la recolección y uso por esa razón específica solamente.
    Si te pedimos tu información personal por una razón secundaria, como marketing, te
    pediremos directamente tu expreso consentimiento y te daremos la oportunidad de negarte.
    </p>
    
    <h1>¿Cómo puedo anular mi consentimiento?</h1>
    <p>
    Si luego de haber aceptado cambias de opinión, puedes anular tu consentimiento para que
    te contactemos, por la recolección, uso o divulgación de tu información, en cualquier
    momento, contactándonos a HUASAR@gmail.com o escribiéndonos a: Amatoli Colonia
    centro, Av.Reloj, Huejutla De Reyes Hidalgo, Hgo, 43080, Mexico
    Enlaces a terceros
    Este sitio web pudiera contener enlaces a otros sitios que pudieran ser de su interés. Una
    vez que usted de clic en estos enlaces y abandone nuestra página, ya no tenemos control
    sobre al sitio al que es redirigido y por lo tanto no somos responsables de los términos o
    privacidad ni de la protección de sus datos en esos otros sitios terceros. Dichos sitios están
    sujetos a sus propias políticas de privacidad por lo cual es recomendable que los consulte
    para confirmar que usted está de acuerdo con estas.
    Control de su información personal
    En cualquier momento usted puede restringir la recopilación o el uso de la información
    personal que es proporcionada a nuestro sitio web. Cada vez que se le solicite rellenar un
    formulario, como el de alta de usuario, puede marcar o desmarcar la opción de recibir
    información por correo electrónico. En caso de que haya marcado la opción de recibir
    nuestro boletín o publicidad usted puede cancelarla en cualquier momento.
    Esta compañía no venderá, cederá ni distribuirá la información personal que es recopilada
    sin su consentimiento, salvo que sea requerido por un juez con un orden judicial.
    Amatoli Se reserva el derecho de cambiar los términos de la presente Política de Privacidad
    en cualquier momento.
    </p>
    <h1>Divulgación</h1>
    <p>
    Podemos divulgar tu información personal si se nos requiere por ley o si violas nuestros
    Términos de Servicio.
    Edad de consentimiento
    Al utilizar este sitio, declaras que tienes al menos la mayoría de edad en tu estado o
    provincia de residencia, o que tienes la mayoría de edad en tu estado o provincia de
    residencia y que nos has dado tu consentimiento para permitir que cualquiera de tus
    dependientes menores use este sitio.
    </p>
    <h1>Cambios a esta política de privacidad</h1>
    <p>
    Nos reservamos el derecho de modificar esta política de privacidad en cualquier momento,
    asique por favor revísala frecuentemente. Cambios y aclaraciones entrarán en vigencia
    inmediatamente después de su publicación en el sitio web. Si hacemos cambios materiales
    a esta política, notificaremos aquí que ha sido actualizada, por lo que cual estás enterado de
    qué información recopilamos, cómo y bajo qué circunstancias, si las hubiere, la utilizamos
    y/o divulgamos.
    Si nuestra tienda es adquirida o fusionada con otra empresa, tu información puede ser
    transferida a los nuevos propietarios, para que podamos seguir vendiéndote productos.
    </p>
    <h1>Ultima fecha de actualización</h1>
    <p>
    26 de septiembre de 2020
    </p>
    </div>    
    <br>
</div>
    
@endsection
