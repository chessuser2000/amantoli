@extends('layouts.plantillahome')
@section('tittle', 'Registro')
@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<div class="container container-formulario" style="margin-block: 10px">
    <form action="/Registrouser" method="POST" enctype="multipart/form-data" role="form">
        {{ csrf_field() }}
    <h1 style="text-align: center;">Crear cuenta</h1>
    <div class="form-row">
        <div class="form-group col-md-6">
        <label for="inputEmail">Correo electrónico</label>
        <div class="input-group mb-2">
        <div class="input-group-prepend">
        <div class="input-group-text"><i class="iconos-amatoli fas fa-envelope"></i></div>
        </div>
            <input type="email" name="correo" class="form-control" placeholder="Correo electronico" id="inputEmail">
        </div>
        </div>
        <div class="form-group col-md-6">
        <label for="inputPassword">Contraseña</label>
        <div class="input-group mb-2">
        <div class="input-group-prepend">
        <div class="input-group-text"><i class="iconos-amatoli fas fa-user-lock"></i></div>
        </div>
            <input type="password" name="contrasenia" class="form-control" id="inputPassword" placeholder="Contraseña">
        </div>
        </div>
    </div>
    <div class="form-group">
        <div class="form-check">
        <input class="form-check-input" type="checkbox" id="gridCheck" required>
        <label class="form-check-label" for="gridCheck">
            Acepto los <a href="">Terminos y condiciones</a><br> y he leido la <a href="/Aviso-de-Privacidad">Politica de privacidad</a>
        </label>
        </div>
    </div>
    <button type="submit" class="btn boton-primario">Registrarme</button>
    </form>
</div>
@endsection