@extends('layouts.plantillahome')
@section('tittle', 'Contacto')
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Inicio</a></li>
          <li class="breadcrumb-item active" aria-current="page">Contacto</li>
        </ol>
    </nav>
    <style>
        .error-message{
            color: red;
        }
    </style>
    <div class="container-fluid contenedor-padd" id="body">
        <div class="container-fluid">
            @if (session('info'))
                <div class="alert alert-success">
                    {{session('info')}}
                </div>
            @endif
            <div class="row">
                <div class="col-md-8" style="margin-block-end: 10px">
                    <h1>Contactanos</h1>
                    <form action="{{route('contactanos.store')}}" method="post">
                        @csrf
                        <label for="idnombre">Nombre</label>
                        <input type="text" name="nombre" placeholder="Nombre Completo" class="form-control" id="idnombre">
                        @error('nombre')
                            <p class="error-message">{{$message}}</p>
                        @enderror
                        <label for="idemail">E-mail</label>
                        <input type="email" name="email" placeholder="E-mail" class="form-control" id="idemail">
                        @error('email')
                            <p class="error-message">{{$message}}</p>
                        @enderror
                        <label for="idmsj">Mensaje</label>
                        <textarea name="mensaje" class="form-control" placeholder="Contenido del mensaje..." id="idmsj" cols="30" rows="10"></textarea>
                        @error('mensaje')
                            <p class="error-message">{{$message}}</p>
                        @enderror
                        <br>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="g-recaptcha" data-sitekey="6LdBeUAaAAAAALlXP_lNkAmbhFMloIlAvhns4gPq"></div>
                            </div>
                        </div>
                        <button type="submit" class="btn boton-primario">Enviar mensaje</button>
                    </form>
                </div>

            <div class="col-md-4">
                <h1>!Siguenos en nuestras redes sociales¡</h1>
                <ul class="social-icons">
                    <li><a class="facebook" href="#"><i class="fab fa-facebook"></i></a></li>
                    <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                    <li><a class="dribbble" href="#"><i class="fab fa-instagram"></i></a></li>
                  </ul>
                  <h3>Dirección</h3>
                    <ul class="footer-links">
                        <li><i class="fas fa-home"></i> Colonia centro, Av. Reloj, Huejutla de Reyes, Hidalgo, 43080, México.</li>
                        <li><i class="fas fa-phone"></i> 7712674902</li>
                        <li><i class="fas fa-phone"></i> 7712390974</li>
                        <li><i class="fas fa-phone"></i> 7711795277</li>
                    </ul>
            </div>
        </div>
    </div>
</div>
@endsection