@extends('layouts.plantillahome')
@section('tittle', 'Información')
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Inicio</a></li>
      <li class="breadcrumb-item active" aria-current="page">¿Quienes somos?</li>
    </ol>
  </nav>
  <div class="container-fluid contenedor-padd" id="body">
    <div class="container">
    <h1>Ubicación</h1>
    <div id="mapid" class="d-none d-sm-none d-md-block" style="width: 950px; height: 450px;"></div>
    <div id="mapid2" class="d-block d-sm-block d-md-none" style="width: 350px; height: 350px;"></div>
    <h1>Historia</h1>
      <p>
        Aquí va la historia de la empresa
      </p>
    <h1>Misión</h1>
      <p>
        Lograr que todos los artesanos de la región en donde operamos tengan acceso a 
        nuestros servicios con el fin de expandir su mercado a un nivel nacional y al 
        mismo tiempo ofrecer una oportunidad de aumento de ganancias hacia ambas partes.
      </p>
    <h1>Visión</h1>
        <p>
          La visión de la empresa Amantoli, será que está lidere el fortalecimiento 
          de la actividad artesanal de nuestra región, contribuyendo al desarrollo local y
          regional y a la preservación de los oficios y la tradición existentes en nuestro país.
        </p>
    <h1>Valores</h1>
        <p>
          Ofrecer una buena calidad de producto a nuestro cliente es lo primordial. 
          Así como hacemos el compromiso de dar a conocer más la cultura artesanal de una 
          manera honesta y responsable a la distribución de sus productos y sobre todo el 
          trabajo en equipo, puesto que no tendrá dificultad alguna para llevar a cabo su labor 
          como artesanos y como empresa y así poder tener la oportunidad del crecimiento.
        </p>    
    </div>
    <br>
  </div>
  <script>

    var mymap = L.map('mapid').setView([21.155687, -98.381047], 15);
  
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      maxZoom: 18,
      attribution: 'Datos del mapa &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contribuyentes, ' +
        'Imagenes © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1
    }).addTo(mymap);
  
    L.marker([21.155687, -98.381047]).addTo(mymap)
      .bindPopup("<b>Hola aquí estamos!</b>.").openPopup();
  
    L.circle([21.155687, -98.381047], 500, {
      color: 'red',
      fillColor: '#f03',
      fillOpacity: 0.5
    }).addTo(mymap);

    var popup = L.popup();
  
    function onMapClick(e) {
      popup
        .setLatLng(e.latlng)
        .setContent("Posicíon: " + e.latlng.toString())
        .openOn(mymap);
    }
  
    mymap.on('click', onMapClick);  
  </script>
  <script>
        var mymap2 = L.map('mapid2').setView([21.155687, -98.381047], 15);
  
  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Datos del mapa &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contribuyentes, ' +
      'Imagenes © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1
  }).addTo(mymap2);

  L.marker([21.155687, -98.381047]).addTo(mymap2)
    .bindPopup("<b>Hola aquí estamos!</b>.").openPopup();

  L.circle([21.155687, -98.381047], 500, {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5
  }).addTo(mymap2);

  var popup = L.popup();

  function onMapClick(e) {
    popup
      .setLatLng(e.latlng)
      .setContent("Posicíon: " + e.latlng.toString())
      .openOn(mymap2);
  }

  mymap2.on('click', onMapClick);
  </script>
@endsection