@extends('layouts.plantillahome')
@section('tittle','Mi Perfil')
@section('content')
<div class="container-fluid" id="body">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <!--
    @if (Auth::user()->Tipo_user=="cliente")
    <p>hola</p>
    @endif
    -->
    @if (Auth::user()->Tipo_user=="admin")
        <a href="{{route('formulario-productos')}}">Crear producto</a>
        <a href="{{route('formularioimg')}}">Imagenes productos</a>
        <a href="{{route('productostienda')}}">Listado de productos</a>
    @endif    
</div>
@endsection
