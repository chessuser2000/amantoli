@extends('layouts.plantillahome')
@section('tittle', 'Puerta de enlace incorrecta')
@section('content')
<section style="padding-top: 100px" id="body">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center">
                <h1 style="font-size: 162px"><img src="multimedia/502.png" alt="" height="500px"></h1>
                <h2>Puerta de enlace incorrecta</h2>
                <a href="/" class="btn btn-primary">Continuar comprando</a>
            </div>
        </div>
    </div>
</section>
@endsection
