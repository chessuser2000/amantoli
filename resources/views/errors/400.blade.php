@extends('layouts.plantillahome')
@section('tittle', 'Petición o Solicitud Incorrecta')
@section('content')
<section style="padding-top: 100px" id="body">
    <div class="container" id="body">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center">
                <h1 style="font-size: 162px"><img src="multimedia/400.png" alt="" height="400px"></h1>
                <h2>Petición o Solicitud Incorrecta</h2>
                <p>Lo sentimos su petición no ha podido ser procesada</p>
                <a href="/" class="btn btn-primary">Continuar comprando</a>
            </div>
        </div>
    </div>
</section>
@endsection
