@extends('layouts.plantillahome')
@section('tittle', 'Ha ocurrido un error')
@section('content')
<section style="padding-top: 10px" id="body">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center">
                <h1 style="font-size: 162px"><img src="{{asset('multimedia/405.png')}}" alt="" height="400px"></h1>
                <h2>Ha ocurrido un error</h2>
                <p>Algo está roto. Háganos saber qué estaba haciendo cuando se produjo este error. Lo arreglaremos lo más pronto posible. Pedimos disculpas por cualquier inconveniente causado. </p>
                <a href="/" class="btn btn-primary">Continuar comprando</a>
            </div>
        </div>
    </div>
    <br>
</section>
@endsection