@extends('layouts.plantillahome')
@section('tittle', 'Error interno del servidor')
@section('content')
<section style="padding-top: 100px" id="body">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center">
                <h1 style="font-size: 162px"><img src="multimedia/500.png" alt="" height="400px"></h1>
                <h2>Error interno del servidor</h2>
                <p>Lo sentimos, ha ocurrido un problema con el servidor</p>
                <a href="/" class="btn btn-primary">Continuar comprando</a>
            </div>
        </div>
    </div>
</section>
@endsection