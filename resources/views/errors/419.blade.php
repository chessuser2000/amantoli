@extends('layouts.plantillahome')
@section('tittle', 'Pagina no expirada')
@section('content')
<section style="padding-top: 10px" id="body">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center">
                <h1 style="font-size: 162px"><img src="{{asset('multimedia/419.png')}}" alt="" height="400px"></h1>
                <h2>Pagína expirada</h2>
                <p>Lo sentimos pero su petición no puede ser procesada</p>
                <a href="/" class="btn btn-primary">Continuar comprando</a>
            </div>
        </div>
    </div>
    <br>
</section>
@endsection