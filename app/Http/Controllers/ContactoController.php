<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactanosMailable;
use Illuminate\Support\Facades\Mail;

class ContactoController extends Controller
{
    public function index(){
        return view('tienda.contacto');
    }
    public function store(Request $request){
        $request->validate([
            'nombre' => 'required',
            'email' => 'required|email',
            'mensaje' => 'required'
        ]);
        $correo = new ContactanosMailable($request->all());
        Mail::to('user951c@gmail.com')->send($correo);
        return redirect()->route('tienda.contacto')->with('info','Mensaje enviado correctamente');
    }
}
