<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productos;
use App\Models\Categorias;
use App\Models\imagenes;
use App\Models\Imagenesprod;

class InicioController extends Controller
{
    public function __invoke()
    {
        $productos = Productos::all()
        ->where('estadoprod','Disponible')->take(5);
        $categorias = Categorias::all();
        $imgs = Imagenesprod::all();
        //$imgs = imagenes::all();
        return view('inicio',compact('productos','imgs'));
    }
    public function showaviso(){
        return view('tienda.avisodeprivacidad');
    }
    public function formcontacto(){
        return view('tienda.contacto');
    }
    public function aboutus(){
        return view('tienda.aboutus');
    }
    public function error(){
        return view('tienda.aboutus');
    }
    public function showformcreateuser(){
        return view('tienda.formregistro');
    }
    public function detallecategoria($idcategoria){
        $productos = Productos::where('intID_Categoria',$idcategoria)
        ->where('estadoprod','Disponible')->paginate(8);
        return view('tienda.listadocategoria',compact('productos'));
    }
}
