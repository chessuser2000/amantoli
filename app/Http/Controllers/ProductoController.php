<?php

namespace App\Http\Controllers;

use App\Models\Categorias;
use App\Models\Imagenesprod;
use Illuminate\Http\Request;
use App\Models\Productos;
use Illuminate\Support\Facades\Storage;

class ProductoController extends Controller
{
    public function showcatalogo(){
        $productos = Productos::paginate(8)->where('estadoprod','Disponible');
        $imgs = Imagenesprod::all();
        return view('Productos.catalogoprod', compact('productos'));
    }
    public function pagination(){
        $productos = Productos::paginate(4)->where('estadoprod','Disponible');
        return view('Productos.pagination', compact('productos'));
    }
    public function autocompletado(Request $request){
        $term=$request->get('term');
        $querys = Productos::where('vchNombre', 'LIKE', '%' . $term . '%')
        ->where('estadoprod','Disponible')->paginate(10);
        $data=[];
        foreach($querys as $query){
            $data[]=[
                'label'=>$query->vchNombre
            ];
        }
        return $data;
    }
    public function buscar(Request $request){
        $request->validate([
            'buscar' => 'required|max:30'
        ]);
        $term=$request->get('buscar');
        $productos = Productos::where('vchNombre', 'LIKE', '%' . $term . '%')
        ->where('estadoprod','Disponible')->paginate(8);
        return view('Productos.resultados',compact('productos'));
    }

    public function detallesprod($idproducto){
        $detprod = Productos::findOrFail($idproducto);
        $productos = Productos::all()
        ->where('estadoprod','Disponible')->take(5);
        $imgs = Imagenesprod::all()->where('idProducto',$idproducto);
        return view('Productos.detalleproducto', compact('detprod','productos','imgs'));
    }
    public function busquedaavanzada(Request $request){
        $request->validate([
            'nombre' => 'required|max:30',
            'precio' => 'required|max:5'
        ]);
        $term1=$request->get('nombre');
        $term2=$request->get('categorias');
        $term3=$request->get('precio');
        if($term3==null){
        $productos = Productos::where('vchnombre','LIKE', '%' .$term1. '%')
        ->where('intID_Categoria',$term2)->where('estadoprod','Disponible')
        ->paginate(8);
        }
        else{
        $productos = Productos::where('vchnombre','LIKE', '%' .$term1. '%')
        ->where('intID_Categoria',$term2)
        ->where('fltPrecioV','<=',$term3)
        ->where('estadoprod','Disponible')->paginate(8);   
        }
        return view('Productos.resultadosavanzados',compact('productos'));
    }

    public function createproduct(Request $request){
        $producto = new Productos();
        $producto -> vchNombre = $request -> nombreprod;
        $producto -> intID_Tienda = $request -> tienda;
        $producto -> intID_Categoria = $request -> categoria;
        $producto -> fltPrecioV = $request -> preciov;
        $producto -> fltGananciaActual = $request -> ganancia;
        $producto -> intExistencia = $request -> existencia;
        $producto -> txtDescripcion = $request -> descripcion;
        $producto -> estadoprod = $request -> estado;
        $producto -> save();
        return redirect()->back()->with('message','El producto se registro correctamente');
    }
    public function eliminarprod($producto){
        $productos = Productos::where('intID_Producto',$producto)->first();;
        $productos->delete();
        return redirect()->back()->with('message','ok');
    }
    public function addimages($producto){
        $productos = Productos::where('intID_Producto',$producto)->first();
        $imgs = Imagenesprod::all()->where('idProducto',$producto);
        return view('admins.formimgproductos',compact('productos','imgs'));
    }
    public function guardarimg(Request $request, $producto){
        $request->validate([
            'imagen'=>'required|image',
        ]);
        $imagen = $request -> file('imagen')->store('public/imagenesproductos');
        $url = Storage::url($imagen);
        Imagenesprod::create([
            'idProducto'=>$producto,
            'vchImagen'=>$url
        ]);
    }
    public function eliminarimg($imagen){
        $imagenesd = Imagenesprod::where('intID_imgprod',$imagen)->first();
        $url = str_replace('storage','public',$imagenesd->imagen);
        Storage::delete($url);
        $imagenesd->delete();
        return redirect()->back()->with('message','ok');
    }
    public function addimgproduct(Request $request){
        $imagen = new Productos();
        $imagen -> vchImagen=$request->imgp;
        $imagen -> vchImagen = $request->file('file')->store('public/imagenesproductos'); 
        return redirect()->back()->with('message','Imagen añadida correctamente');
    }
    public function showallproducts(){
        $productos = Productos::all();  
        return view('admins.showallproducts',compact('productos'));
    }
    public function showcarrito(){
        return view('Productos.carrito');
    }
}
