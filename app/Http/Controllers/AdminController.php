<?php

namespace App\Http\Controllers;

use App\Models\Categorias;
use App\Models\Productos;
use App\Models\Tiendas;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function showformproducts(){
        $categorias = Categorias::all();
        $tiendas = Tiendas::all();
        return view('admins.formproductos',compact('categorias','tiendas'));
    }
    public function showformimgproducts(){
        $productos = Productos::all();
        return view('admins.formimgproductos',compact('productos'));
    }
}
