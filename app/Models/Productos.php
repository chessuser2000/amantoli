<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $table = 'tbl_productos';
    protected $primaryKey = 'intID_Producto';
    public $timestamps = false;
}
