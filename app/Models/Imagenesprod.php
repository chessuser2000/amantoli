<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Imagenesprod extends Model
{
    use HasFactory;
    protected $table = 'tbl_imagenes_productos';
    protected $primaryKey = 'intID_imgprod';
    public $timestamps = false;
    protected $fillable=['vchImagen','idProducto']; 
}
