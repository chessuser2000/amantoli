<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tiendas extends Model
{
    protected $table='tbl_tienda';
    protected $primaryKey = 'intID_Tienda';
    public $timestamps = false;
}
