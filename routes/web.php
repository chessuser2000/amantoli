<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ContactoController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductoController;
use App\Mail\ContactanosMailable;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\InicioController;
use App\Models\Productos;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',InicioController::class);
Route::get('avisodeprivacidad',[InicioController::class, 'showaviso']);
Route::get('contacto',[InicioController::class, 'formcontacto'])->middleware(['password.confirm']);
Route::get('nosotros',[InicioController::class, 'aboutus'])->name('aboutus');
Route::get('registro',[InicioController::class, 'showformcreateuser']);
Route::get('/detallecat/{idcategoria}', [InicioController::class, 'detallecategoria'])->name('detallecat');

Route::get('tienda',[ProductoController::class, 'showcatalogo'])->name('tienda');
Route::get('tienda/pagination',[ProductoController::class, 'pagination']);
Route::get('autocompletar/productos',[ProductoController::class, 'autocompletado']);
Route::get('buscar/productos',[ProductoController::class, 'buscar']);
Route::get('/detalleproducto/{clvprod}',[ProductoController::class,'detallesprod'])->name('detalleproducto');
Route::get('/buscar/busquedaavanzada',[ProductoController::class,'busquedaavanzada']);
Route::get('/carrito',[ProductoController::class,'showcarrito'])->name('carrito');

Route::get('contactanos', [ContactoController::class, 'index'])->name('tienda.contacto');
Route::post('contactanos', [ContactoController::class, 'store'])->name('contactanos.store');

Route::get('formularioproductos',[AdminController::class,'showformproducts'])->name('formulario-productos')->middleware(['auth','password.confirm']);
Route::post('/createproducto', [ProductoController::class,'createproduct'])->middleware(['auth']);
Route::get('formularioimgprod',[AdminController::class,'showformimgproducts'])->name('formularioimg')->middleware('auth');
Route::post('/addimage', [ProductoController::class,'addimgproduct'])->middleware('auth');
Route::get('/showproductos',[ProductoController::class,'showallproducts'])->name('productostienda')->middleware('auth');
Route::delete('eliminarprod/{id}',[ProductoController::class,'eliminarprod'])->name('eliminarprod');
Route::get('addimages/{id}',[ProductoController::class,'addimages'])->name('addimages');

Route::post('/guardarimg/{id}',[ProductoController::class,'guardarimg'])->name('guardarimg')->middleware('auth');
Route::post('/guardarimgp/{id}',[ProductoController::class,'addimgproduct'])->name('guardarimgp')->middleware('auth');
Route::delete('eliminarimg/{id}',[ProductoController::class,'eliminarimg'])->name('eliminarimg')->middleware('auth');
//Route::get('/home',[HomeController::class, 'index'])->name('home')->middleware(['auth','verified']);
Auth::routes(['verify' => true]);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware(['auth','verified']);


