var tema = document.getElementById('tema');
var cuerpo = document.getElementById('body');
var titulos = document.getElementsByClassName('subtitulo');
var cartas = document.getElementsByClassName('card');
var cartatext = document.getElementsByClassName('card-texto'); 
var navbar = document.getElementsByClassName('navbar');
var footerclass = document.getElementsByClassName('footerclass');
var comentarios = document.getElementsByClassName('comentarios');
var tablas = document.getElementById('productos');
var theme = document.getElementById('result');

storagetheme = localStorage.getItem("storagetheme");

if(storagetheme == 'oscuro'){
  result.innerHTML += '<i class="fas fa-moon them-claro"></i>';
  $(cuerpo).addClass("black" );
  $(titulos).addClass( "titulos-black" );
  $(cartas).addClass( "cartas-black" );
  $(cartatext).addClass("texto-card-black");
  $(navbar).addClass("navbar-dark bg-dark");
  $(footerclass).addClass("footer-black");
  $(comentarios).addClass("comentarios-black");
  $(tablas).addClass("table-darks");
}else{result.innerHTML += '<i class="fas fa-sun them-claro"></i>'}

$(tema).click(function() {
  $( cuerpo ).toggleClass( "black" );
  $(titulos).toggleClass( "titulos-black" );
  $(cartas).toggleClass( "cartas-black" );
  $(cartatext).toggleClass("texto-card-black");
  $(navbar).toggleClass("navbar-dark bg-dark");
  $(footerclass).toggleClass("footer-black");
  $(comentarios).toggleClass("comentarios-black");
  $(tablas).toggleClass("table-darks");
  revisartema();
});
function revisartema(){
storagetheme = localStorage.getItem("storagetheme");
if(storagetheme == 'oscuro'){
    localStorage.setItem("storagetheme", "claro");
    result.innerHTML = '<i class="fas fa-sun them-claro"></i>';
    return false;
}
if(storagetheme == 'claro'){
    localStorage.setItem("storagetheme", "oscuro");
}
if(storagetheme == null){
    localStorage.setItem("storagetheme", "oscuro");
}
    result.innerHTML = '<i class="fas fa-moon them-claro"></i>';

}